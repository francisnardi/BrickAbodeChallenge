# 1. Example of F# REST API that saves files in memory (UseInMemoryDatabase .NET 2.2)

## Pre-requisites
* The [.NET Core SDK](https://www.microsoft.com/net/download) 2.2.

## Starting the application
Before you run the project, you must:

```bash
dotnet restore
```

To run the server, use the following command:

```bash
dotnet run
```

Then test the API using Postman / curl (or even your browser, e.g., for GET calls):

https://localhost:5001/api/values

https://localhost:5001/api/todoitems