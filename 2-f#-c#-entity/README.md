# 2. F# + C# data layer using Entity Framework Core

## Pre-requisites
* The [.NET Core SDK](https://www.microsoft.com/net/download) 2.2.
* A set up and running PostgreSQL database (https://www.postgresql.org/download/).

## Preparing the application database
To create the migration:

```bash
dotnet ef migrations add InitialCreate -p .\FSharpData.Migrations\
```

To create or to update the database:

```bash
dotnet ef database update -p .\FSharpData.Migrations\
```

## Starting the application
Before you run the project, you must:

```bash
dotnet restore
```

To run the server, use the following command:

```bash
dotnet run -p .\FSharpData.API\
```

Then test the API using Postman / curl (or even your browser, e.g., for GET calls):

POST https://localhost:5000/api/users
(To create a question)
EXAMPLE BODY
{
	"Email": "email0mail.com",
	"Username": "Name"
}

GET https://localhost:5000/api/users

GET https://localhost:5000/api/users/1

GET https://localhost:5000/api/users/{id}


POST https://localhost:5000/api/questions
(To create a question)
EXAMPLE BODY
{
	"Title": "First Question",
	"Text": "This is the body of the first question",
    "UserId": 1
}

GET https://localhost:5000/api/questions

GET https://localhost:5000/api/questions/1

GET https://localhost:5000/api/questions/{id}
