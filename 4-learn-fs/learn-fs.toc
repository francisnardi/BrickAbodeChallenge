\contentsline {section}{\numberline {1}Functional programming}{3}{}%
\contentsline {subsection}{\numberline {1.1}Pure functions}{3}{}%
\contentsline {subsection}{\numberline {1.2}Side effects}{3}{}%
\contentsline {section}{\numberline {2}.fsx vs .fs files}{3}{}%
\contentsline {section}{\numberline {3}Type annotations}{3}{}%
\contentsline {section}{\numberline {4}Primitives}{4}{}%
\contentsline {subsection}{\numberline {4.1}Mutable/Assignment}{4}{}%
\contentsline {section}{\numberline {5}Expressions vs Statements}{5}{}%
\contentsline {section}{\numberline {6}Functions}{5}{}%
\contentsline {subsection}{\numberline {6.1}Lambda expressions}{5}{}%
\contentsline {subsection}{\numberline {6.2}Currying/Baking-In }{5}{}%
\contentsline {section}{\numberline {7}Partial Application}{5}{}%
\contentsline {section}{\numberline {8}Function composition}{6}{}%
\contentsline {section}{\numberline {9}Pipe operator}{6}{}%
\contentsline {section}{\numberline {10}Composition operator}{6}{}%
\contentsline {section}{\numberline {11}Defining new operators}{7}{}%
\contentsline {section}{\numberline {12}Hello World/Main function}{7}{}%
\contentsline {section}{\numberline {13}Unit}{7}{}%
\contentsline {section}{\numberline {14}Printing to Console}{8}{}%
\contentsline {section}{\numberline {15}Pattern Matching}{8}{}%
\contentsline {section}{\numberline {16}The "function" keyword}{9}{}%
\contentsline {section}{\numberline {17}Pattern matching with let and fun}{9}{}%
\contentsline {section}{\numberline {18}Option type}{9}{}%
\contentsline {section}{\numberline {19}Domain Errors vs Exceptions}{9}{}%
\contentsline {section}{\numberline {20}Generics and SRTP}{9}{}%
\contentsline {section}{\numberline {21}Inline keyword}{9}{}%
\contentsline {section}{\numberline {22}Type members}{10}{}%
\contentsline {section}{\numberline {23}Collections}{10}{}%
\contentsline {section}{\numberline {24}Arrays}{10}{}%
\contentsline {section}{\numberline {25}Lists}{10}{}%
\contentsline {section}{\numberline {26}Collection libraries}{10}{}%
\contentsline {section}{\numberline {27}List.head}{10}{}%
\contentsline {section}{\numberline {28}Recursion / List.iter}{10}{}%
\contentsline {section}{\numberline {29}List.map}{10}{}%
\contentsline {section}{\numberline {30}List.fold}{10}{}%
\contentsline {section}{\numberline {31}List.reduce}{10}{}%
\contentsline {section}{\numberline {32}List.sum}{11}{}%
\contentsline {section}{\numberline {33}Bind}{11}{}%
\contentsline {section}{\numberline {34}Exception handling}{11}{}%
\contentsline {section}{\numberline {35}Results / Error Modeling}{11}{}%
\contentsline {section}{\numberline {36}Outtro}{11}{}%
