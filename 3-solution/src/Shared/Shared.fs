namespace Shared

open System

//type Todo =
//    { Id : Guid
//      Description : string }

type Provider =
    { Id : Guid
      ProviderName : string }

type Currency =
    { Id : Guid
      CurrencyName : string }

type Stock =
    { Id : Guid
      StockName : string }

type Screen1 =
    { Id: Guid
      // Provider
      ProviderId: Guid
      // Pair
      CurrencyId1: Guid
      CurrencyId2: Guid
      // Hour
      Hour: DateTime
      // Graph Outputs
      Feed: Double
      Vwap: Double
      Overall: Double }

//module Todo =
//    let isValid (description: string) =
//        String.IsNullOrWhiteSpace description |> not

//    let create (description: string) =
//        { Id = Guid.NewGuid()
//          Description = description }

module Provider =
    let isValid (pName: string) =
        String.IsNullOrWhiteSpace pName |> not

    let create (pName: string) =
        { Id = Guid.NewGuid()
          ProviderName = pName }


module Route =
    let builder typeName methodName =
        sprintf "/api/%s/%s" typeName methodName

//type ITodosApi =
//    { getTodos : unit -> Async<Todo list>
//      addTodo : Todo -> Async<Todo> }

type IProvidersApi =
    { getProviders : unit -> Async<Provider list>
      addProvider : Provider -> Async<Provider> }