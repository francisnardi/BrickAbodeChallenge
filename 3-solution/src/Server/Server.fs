module Server

open Fable.Remoting.Server
open Fable.Remoting.Giraffe
open Saturn

open Shared

type Storage () =
    //let todos = ResizeArray<_>()

    //member __.GetTodos () =
    //    List.ofSeq todos

    let providers = ResizeArray<_>()

    member __.GetProviders () =
        List.ofSeq providers

    //member __.AddTodo (todo: Todo) =
    //    if Todo.isValid todo.Description then
    //        todos.Add todo
    //        Ok ()
    //    else Error "Invalid todo"

    member __.AddProvider (provider: Provider) =
        if Provider.isValid provider.ProviderName then
            providers.Add provider
            Ok ()
        else Error "Invalid provider"

let storage = Storage()

storage.AddProvider(Provider.create "NYSE") |> ignore
storage.AddProvider(Provider.create "DJIA") |> ignore
storage.AddProvider(Provider.create "CBOE") |> ignore

let providersApi =
    { getProviders = fun () -> async { return storage.GetProviders() }
      addProvider =
        fun provider -> async {
            match storage.AddProvider provider with
            | Ok () -> return provider
            | Error e -> return failwith e
        } }

let webApp =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.fromValue providersApi
    |> Remoting.buildHttpHandler

let app =
    application {
        url "http://0.0.0.0:8085"
        use_router webApp
        memory_cache
        use_static "public"
        use_gzip
    }

run app
