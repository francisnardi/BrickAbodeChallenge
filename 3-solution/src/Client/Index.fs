module Index

open Elmish
open Fable.Remoting.Client
open Shared

type Model =
    { Providers: Provider list
      Input: string }

type Msg =
    | GotProviders of Provider list
    | SetInput of string
    | AddProvider
    | AddedProvider of Provider

let providersApi =
    Remoting.createApi()
    |> Remoting.withRouteBuilder Route.builder
    |> Remoting.buildProxy<IProvidersApi>

let init(): Model * Cmd<Msg> =
    let model =
        { Providers = []
          Input = "" }
    let cmd = Cmd.OfAsync.perform providersApi.getProviders () GotProviders
    model, cmd

let update (msg: Msg) (model: Model): Model * Cmd<Msg> =
    match msg with
    | GotProviders providers ->
        { model with Providers = providers }, Cmd.none
    | SetInput value ->
        { model with Input = value }, Cmd.none
    | AddProvider ->
        let provider = Provider.create model.Input
        let cmd = Cmd.OfAsync.perform providersApi.addProvider provider AddedProvider
        { model with Input = "" }, cmd
    | AddedProvider provider ->
        { model with Providers = model.Providers @ [ provider ] }, Cmd.none

open Fable.React
open Fable.React.Props
open Fulma

let navBrand =
    Navbar.Brand.div [ ] [
        Navbar.Item.a [
            Navbar.Item.Props [ Href "https://safe-stack.github.io/" ]
            Navbar.Item.IsActive true
        ] [
            img [
                Src "/favicon.png"
                Alt "Logo"
            ]
        ]
    ]

let containerBox (model : Model) (dispatch : Msg -> unit) =
    Box.box' [ ] [
        Content.content [ ] [
            Content.Ol.ol [ ] [
                for provider in model.Providers do
                    li [ ] [ str provider.ProviderName ]
            ]
        ]
        Field.div [ Field.IsGrouped ] [
            Control.p [ Control.IsExpanded ] [
                Input.text [
                  Input.Value model.Input
                  Input.Placeholder "Add the provider you want?"
                  Input.OnChange (fun x -> SetInput x.Value |> dispatch) ]
            ]
            Control.p [ ] [
                Button.a [
                    Button.Color IsPrimary
                    Button.Disabled (Provider.isValid model.Input |> not)
                    Button.OnClick (fun _ -> dispatch AddProvider)
                ] [
                    str "Add"
                ]
            ]
        ]
    ]

let view (model : Model) (dispatch : Msg -> unit) =
    Hero.hero [
        Hero.Color IsPrimary
        Hero.IsFullHeight
        Hero.Props [
            Style [
                Background """linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("https://unsplash.it/1200/900?random") no-repeat center center fixed"""
                BackgroundSize "cover"
            ]
        ]
    ] [
        Hero.head [ ] [
            Navbar.navbar [ ] [
                Container.container [ ] [ navBrand ]
            ]
        ]

        Hero.body [ ] [
            Container.container [ ] [
                Column.column [
                    Column.Width (Screen.All, Column.Is6)
                    Column.Offset (Screen.All, Column.Is3)
                ] [
                    Heading.p [ Heading.Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ] [ str "SAFE" ]
                    containerBox model dispatch
                ]
            ]
        ]
    ]
