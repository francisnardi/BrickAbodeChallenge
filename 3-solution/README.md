# 3. Brick Abode (Palladris) Challenge Solution

## Pre-requisites
* The [.NET Core SDK](https://www.microsoft.com/net/download) 3.1 or higher.
* [npm](https://nodejs.org/en/download/) package manager.
* [Node LTS](https://nodejs.org/en/download/).

## Starting the application
Before you run the project **for the first time only** you must install dotnet "local tools" with this command:

```bash
dotnet tool restore
```

To concurrently run the server and the client components in watch mode use the following command:

```bash
dotnet fake build -t run
```

Then open `http://localhost:8080` in your browser.

To run concurrently server and client tests in watch mode (run in a new terminal):

```bash
dotnet fake build -t runtests
```

Client tests are available under `http://localhost:8081` in your browser and server tests are running in watch mode in console.

## Evidence of the working API

![getProviders](https://gitlab.com/francisnardi/BrickAbodeChallenge/-/raw/master/3-solution/images/01-getProviders.PNG?inline=true)

![addProvider](https://gitlab.com/francisnardi/BrickAbodeChallenge/-/raw/master/3-solution/images/02-addProvider.PNG?inline=true)
