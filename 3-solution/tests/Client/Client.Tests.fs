module Client.Tests

open Fable.Mocha

open Index
open Shared

let client = testList "Client" [
    testCase "Added provider" <| fun _ ->
        let newProvider = Provider.create "new provider"
        let model, _ = init ()

        let model, _ = update (AddedProvider newProvider) model

        Expect.equal 1 model.Providers.Length "There should be 1 provider"
        Expect.equal newProvider model.Providers.[0] "Provider should equal new provider"
]

let all =
    testList "All"
        [
#if FABLE_COMPILER // This preprocessor directive makes editor happy
            Shared.Tests.shared
#endif
            client
        ]

[<EntryPoint>]
let main _ = Mocha.runTests all