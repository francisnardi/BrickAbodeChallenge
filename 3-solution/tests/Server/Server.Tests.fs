module Server.Tests

open Expecto

open Shared
open Server

let server = testList "Server" [
    testCase "Adding valid Provider" <| fun _ ->
        let storage = Storage()
        let validProvider = Provider.create "TODO"
        let expectedResult = Ok ()

        let result = storage.AddProvider validProvider

        Expect.equal result expectedResult "Result should be ok"
        Expect.contains (storage.GetProviders()) validProvider "Storage should contain new provider"
]

let all =
    testList "All"
        [
            Shared.Tests.shared
            server
        ]

[<EntryPoint>]
let main _ = runTests defaultConfig all